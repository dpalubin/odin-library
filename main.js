const myLibrary = [];
const bookContainer = document.querySelector('.book-container');
const newBookButton = document.querySelector('.new-book-button');
const modalBackground = document.querySelector('.modal');
const modalBox = document.querySelector('.modal-box');
const submitButton = document.querySelector('.submit-button');
const cancelButton = document.querySelector('.cancel-button');
const titleInput = document.querySelector('.title-input');
const authorInput = document.querySelector('.author-input');
const pagesInput = document.querySelector('.pages-input');
const readInput = document.querySelector('#read-yes');
const inputs = Array.from(document.querySelectorAll('input[type="text"]'));
const warningBox = document.querySelector('.warning');

class Book{
  #title;
  #author;
  #pages;
  #read;

  constructor(title, author, pages, read){
    this.#title = title;
    this.#author = author;
    this.#pages = pages;
    this.#read = read;
  }

  info() {
    let infoString = '(';
    if(Number(this.pages)) infoString += `${this.pages} pages, `;
    infoString += (this.read) ? "read already" : "not read yet";
    infoString += ')';
    return infoString;
  }

  get title(){
    return this.#title;
  }

  get author(){
    return this.#author;
  }

  get pages(){
    return this.#pages;
  }

  get read(){
    return this.#read;
  }

  set read(value){
    this.#read = Boolean(value);
  }
}

function generateColor(){
  let colorString = 'rgb('
  for(let i = 0;i < 3; i++){
    colorString += Math.floor(Math.random() * 256);
    colorString += (i==2) ? ')' : ',';
  }
  return colorString;
}

function randomTextColor(){
  switch(Math.floor(Math.random()*4)){
    case 0:
      return 'rgb(0,0,0)';        //Black
    case 1:
      return 'rgb(192,192,192)';  //Silver
    case 2:
      return 'rgb(255,215,0)';    //Gold
    case 3:
      return 'rgb(255,255,255)';  //White
    default:
      return 'pink';
  }
}

function colorDifference(colorString1,colorString2){
  const rgb1 = colorString1.slice(4,-1).split(',');
  const rgb2 = colorString2.slice(4,-1).split(',');
  let difference = 0;
  for(let i = 0;i < 3;i++){
    difference += Math.abs(rgb1[i] - rgb2[i]);
  }
  console.log(difference);
  return difference;
}

function darken(colorString, value){
  let rgb = colorString.slice(4,-1).split(',');
  rgb[0] = Math.max(rgb[0] - value, 0);
  rgb[1] = Math.max(rgb[1] - value, 0);
  rgb[2] = Math.max(rgb[2] - value, 0);

  return `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`;
}

function addBookToLibrary(book){
  const textColor = randomTextColor();
  let backgroundColor = generateColor();

  while(colorDifference(textColor,backgroundColor) < 250){
    backgroundColor = generateColor();   //Ensure good contrast between text color and book cover
  }

  /* Book div structure:
  div .book-slot
    div .book
      h2 title
      h3 author
      p (pages, read/not)
      img (book pages graphic)
    button (remove book)
    button (read toggle)
*/
  const bookSlot = document.createElement('div');
  bookSlot.classList.add('book-slot');
  const bookDiv = document.createElement('div');
  bookDiv.classList.add('book');
  bookDiv.style['color'] = textColor;
  const bookTitle = document.createElement('h2');
  bookTitle.textContent = book.title;
  bookDiv.appendChild(bookTitle);
  const bookAuthor = document.createElement('h3');
  bookAuthor.textContent = book.author;
  bookDiv.appendChild(bookAuthor);
  const bookInfo = document.createElement('p');
  bookInfo.textContent = book.info();
  const bookPages = document.createElement('img');
  bookPages.src='./images/book_pages.jpg';
  bookPages.classList.add('pages-graphic');
  bookDiv.appendChild(bookPages);
  bookDiv.style['background'] = `linear-gradient(${backgroundColor},${darken(backgroundColor, 150)})`;
  bookDiv.style['width'] = `${60 + (Math.floor(Math.random()*20))}%`;
  bookDiv.style['margin-left'] = `${Math.floor(Math.random()*7)}%`;
  bookDiv.appendChild(bookInfo);
  bookSlot.appendChild(bookDiv);
  const removeButton = document.createElement('button');
  removeButton.classList.add('remove-button');
  removeButton.textContent = "Remove";
  const toggleButton = document.createElement('button');
  toggleButton.classList.add('toggle-button');
  toggleButton.textContent = "Read Book";
  toggleButton.addEventListener('click',(e) => {
    book.read = !book.read;
    bookInfo.textContent = book.info();

    /*
    const slot = e.target.parentNode;
    myLibrary[slot.dataset.index].read = !myLibrary[slot.dataset.index];
    slot.querySelector('p').textContent = myLibrary[slot.dataset.index].info();
    */
  });
  removeButton.addEventListener('click',(e) => {
    myLibrary.splice(bookSlot.dataset.index,1);
    const slots = bookContainer.children;
    for(let i = bookSlot.dataset.index; i < slots.length;i++){
      slots[i].dataset.index--;
    }
    bookContainer.removeChild(bookSlot);
  });
  bookSlot.appendChild(removeButton);
  bookSlot.appendChild(toggleButton);
  bookContainer.appendChild(bookSlot);
  bookSlot.dataset.index = myLibrary.length;
}

function addAllBooksToLibrary(){
  for (const book of myLibrary){
    addBookToLibrary(book);
  }
}

const theHobbit = new Book('The Hobbit','J.R.R. Tolkein',295,true);

addBookToLibrary(theHobbit);
myLibrary.push(theHobbit);

modalBackground.addEventListener('click',() => modalBackground.style['display'] = 'none');
cancelButton.addEventListener('click',() => modalBackground.style['display'] = 'none');
newBookButton.addEventListener('click',() => modalBackground.style['display'] = 'flex');

modalBox.addEventListener('click', (e) => e.stopPropagation());

submitButton.addEventListener('click',(e) => {
  let exitFlag = false;
  if(!authorInput.value){
    authorInput.classList.add('no-text');
    exitFlag = true;
  }
  if(!titleInput.value){
    titleInput.classList.add('no-text');
    exitFlag = true;
  }
  if(exitFlag){
    warningBox.style['display'] = 'block';
    return;
  } 
  const newBook = new Book(
    titleInput.value,
    authorInput.value,
    pagesInput.value,
    readInput.checked
  );
  addBookToLibrary(newBook);
  myLibrary.push(newBook);
  modalBackground.style['display'] = 'none';
  warningBox.style.display = 'none';
});

for(const element of inputs){
  element.addEventListener('input',()=>{
    if(element.value){
      element.classList.remove('no-text');
    }
    else{
      element.classList.add('no-text');
    }
  });
}